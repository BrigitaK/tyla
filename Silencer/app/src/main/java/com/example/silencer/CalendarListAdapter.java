package com.example.silencer;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

public class CalendarListAdapter extends ArrayAdapter<CalendarInstance> {

    private Context _context;
    int _resource;
    FragmentManager _fragmentManager;
    public static final String EXTRA_MESSAGE = "com.example.silencer.MESSAGE";

    public CalendarListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CalendarInstance> objects, FragmentManager fragmentManager) {
        super(context, resource, objects);
        _context = context;
        _resource = resource;
        _fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String account = getItem(position).Account;
        String display = getItem(position).Display;
        int Id =  getItem(position).Id;

        LayoutInflater inflater = LayoutInflater.from(_context);
        convertView = inflater.inflate(_resource, parent, false);

        TextView Display = (TextView) convertView.findViewById(R.id.calendar1);
        TextView Account = (TextView) convertView.findViewById(R.id.calendar2);
        Display.setText(display);
        Account.setText(account);
        Display.setTag(Id);
        Display.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intt = new Intent(_context, CalendarEvents.class);
                intt.putExtra(EXTRA_MESSAGE, view.getTag().toString() );
                _context.startActivity(intt);
            }
        });
        Account.setTag(Id);
        Account.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intt = new Intent(_context, CalendarEvents.class);
                intt.putExtra(EXTRA_MESSAGE, view.getTag().toString() );
                _context.startActivity(intt);
            }
        });

        return convertView;
    }

}


