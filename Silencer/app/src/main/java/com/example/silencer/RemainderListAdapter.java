package com.example.silencer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

public class RemainderListAdapter extends ArrayAdapter<Remainder> {

    private  Context _context;
    int _resource;
    FragmentManager _fragmentManager;
    public RemainderListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Remainder> objects, FragmentManager fragmentManager) {
        super(context, resource, objects);
        _context = context;
        _resource = resource;
        _fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent ) {
        String name = getItem(position).Title();
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String start = dt1.format( getItem(position).Begin() );
        String end = dt1.format( getItem(position).End() );
        LayoutInflater inflater = LayoutInflater.from(_context);
        convertView = inflater.inflate(_resource, parent, false);


        TextView Name = (TextView) convertView.findViewById(R.id.remaider1);
        TextView Start = (TextView) convertView.findViewById(R.id.remaider2);
        TextView End = (TextView) convertView.findViewById(R.id.remaider3);
        Name.setText(name);
        Start.setText(start);
        End.setText(end);
        Name.setTag(R.id.remainder_end, end);
        Name.setTag(R.id.remainder_title, name);
        Name.setTag(R.id.remainder_start, start);
        Name.setTag(R.id.remainder_id, getItem(position).Id());
        Name.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AssignDialog(view);
            }
        });
        Start.setTag(R.id.remainder_end, end);
        Start.setTag(R.id.remainder_title, name);
        Start.setTag(R.id.remainder_start, start);
        Start.setTag(R.id.remainder_id, getItem(position).Id());
        Start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AssignDialog(view);
            }
        });
        End.setTag(R.id.remainder_end, end);
        End.setTag(R.id.remainder_title, name);
        End.setTag(R.id.remainder_start, start);
        End.setTag(R.id.remainder_id, getItem(position).Id());
        End.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AssignDialog(view);
            }
        });
        return convertView;
    }

    public void AssignDialog(View view){
        String end = view.getTag(R.id.remainder_end).toString();
        String Title = view.getTag(R.id.remainder_title).toString();
        String Begin = view.getTag(R.id.remainder_start).toString();
        String Id = view.getTag(R.id.remainder_id).toString();
        InfoDialog assignDialog = InfoDialog.newInstance(Title, Begin, end, Id);
        assignDialog.show(_fragmentManager, "fragment_unassign_event");
    }
}
