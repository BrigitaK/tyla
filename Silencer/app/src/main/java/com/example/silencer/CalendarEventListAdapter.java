package com.example.silencer;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

public class CalendarEventListAdapter extends ArrayAdapter<CalendarEventInstance> {

    private Context _context;
    int _resource;
    FragmentManager _fragmentManager;
    public static final String EXTRA_MESSAGE = "com.example.silencer.MESSAGE";

    public CalendarEventListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CalendarEventInstance> objects, FragmentManager fragmentManager) {
        super(context, resource, objects);
        _context = context;
        _resource = resource;
        _fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String title = getItem(position).Title;
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String start = dt1.format( getItem(position).Begin );
        String end = dt1.format( getItem(position).End );
        int id  = getItem(position).Id;

        LayoutInflater inflater = LayoutInflater.from(_context);
        convertView = inflater.inflate(_resource, parent, false);

        TextView Title = (TextView) convertView.findViewById(R.id.remaider1);
        TextView Begin = (TextView) convertView.findViewById(R.id.remaider2);
        TextView End = (TextView) convertView.findViewById(R.id.remaider3);
        Title.setText(title);
        Title.setTag(id);
        Title.setId(id);
        Title.setTag(R.id.event_Id, id);
        Title.setTag(R.id.event_Name, title);
        Title.setTag(R.id.event_Type, 1);
        Title.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AssignDialog(view);
            }
        });
        Begin.setText(start);
        Begin.setTag(id);
        Begin.setId(id);
        Begin.setTag(R.id.event_Id, id);
        Begin.setTag(R.id.event_Name, title);
        Begin.setTag(R.id.event_Type, 1);
        Begin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AssignDialog(view);
            }
        });
        End.setText(end);
        End.setTag(id);
        End.setId(id);
        End.setTag(R.id.event_Id, id);
        End.setTag(R.id.event_Name, title);
        End.setTag(R.id.event_Type, 1);
        End.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AssignDialog(view);
            }
        });

        /*
                Button btn = new Button(this);
                btn.setId(EVENT_ID);
                SimpleDateFormat dt1 = new SimpleDateFormat("EEE, MMM d, yyyy HH:mm");
                btn.setTag(R.id.event_Id, EVENT_ID);
                btn.setTag(R.id.event_Name, TITLE);
                btn.setText(TITLE + " " + dt1.format(begin) + " - " + dt1.format(end));
                btn.setTag(R.id.event_Type, 1);
                btn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        AssignDialog(view);
                    }
                });

                btn.setTextColor(Color.parseColor("#FFD8E9F2"));
                btn.setBackgroundColor(Color.parseColor("#324C6566"));
                btn.setTextSize(12);
                btn.setGravity(Gravity.START);
                btn.setPadding(10, 1, 5, 0);
                LinearLayoutCompat.LayoutParams lp = new LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
                ll.addView(btn, lp);
                int[] colors = {0, Color.parseColor("#B3C5CE"), 0}; // red for the example
                ll.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
                ll.setDividerDrawable(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, colors));

                  Button btn = new Button(this);
            btn.setId(EVENT_ID);
            btn.setTag(R.id.event_Id, EVENT_ID);
            btn.setTag(R.id.event_Name, TITLE);
            btn.setTag(R.id.event_Type, 0);
            btn.setText(TITLE );
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AssignDialog(view);
                }
            });
            btn.setTextColor(Color.parseColor("#FFD8E9F2"));
            btn.setBackgroundColor(Color.parseColor("#324C6566"));
            btn.setTextSize(12);
            btn.setGravity(Gravity.START);
            btn.setPadding(10, 1, 5, 0);
            LinearLayoutCompat.LayoutParams lp = new LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);

            ll.addView(btn, lp);
            int[] colors = {0, Color.parseColor("#B3C5CE"), 0}; // red for the example
            ll.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            ll.setDividerDrawable(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, colors));

*/
        return convertView;
    }

    public void AssignDialog(View view){
        String id = view.getTag(R.id.event_Id).toString();
        String name = view.getTag(R.id.event_Name).toString();
        String type = view.getTag(R.id.event_Type).toString();

        AssignDialog assignDialog = AssignDialog.newInstance(name, id, type);
        assignDialog.show(_fragmentManager, "fragment_assign_event");
    }

}
