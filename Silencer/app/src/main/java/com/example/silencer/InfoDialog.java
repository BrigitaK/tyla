package com.example.silencer;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class InfoDialog extends DialogFragment {
    DatabaseHandler databaseHandler;

    public static InfoDialog newInstance(String title, String start, String end, String id) {

        InfoDialog frag = new InfoDialog();
        Bundle args = new Bundle();
        args.putString("Title", title);
        args.putString("start", start);
        args.putString("end", end);
        args.putString("id", id);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        databaseHandler = new DatabaseHandler(Remainders.getContextOfApplication());
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.dialogStyle);
        final String title = getArguments().getString("Title");
        final String  ids = getArguments().getString("id");
        final String  start = getArguments().getString("start");
        final String  end = getArguments().getString("end");

        builder.setTitle(getResources().getString(R.string.unassign) + " \"" + title + "\"");

        builder.setMessage(getResources().getString(R.string.beginning) + ": " + start + " " + getResources().getString(R.string.ending)  + ": " + end )
                .setPositiveButton( getResources().getString(R.string.assigneventyes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        UnassignEvent(ids);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.assigneventno), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.dismiss();
                    }
                });


        // Create the AlertDialog object and return it
        AlertDialog alert = builder.create();
        alert.show();
        alert.getWindow().getAttributes();

        TextView textView = alert.findViewById(android.R.id.message);
        textView.setTextSize(20);
        return  alert;
    }

    private void UnassignEvent(String ID){
        databaseHandler.deleteEntry(ID);
       // Toast.makeText( Remainders.getContextOfApplication(), getResources().getString(R.string.deleted) , Toast.LENGTH_LONG).show();

        Intent intt = new Intent(Remainders.getContextOfApplication(), Remainders.class);
        startActivity(intt);

    }


}