package com.example.silencer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Remainder {

    private  int id;
    private  int event;
    private  String title;
    private  Date begin;
    private  Date end;

    public Remainder(){
        event = 0;
        title = "";
        begin =  new Date();;
        end = new Date();
    }
    public Remainder( int _event, String _title, Date _begin, Date _end){
        event = _event;
        title = _title;
        begin =  _begin;
        end = _end;
    }

    public  int Id(){
        return id;
    }
    public  int Event(){
        return event;
    }
    public  String Title(){
        return title;
    }
    public Date Begin(){
        return begin;
    }
    public Date End(){
        return end;
    }

    public  void SetId(int i){
        id = i;
    }
    public  void SetEvent(int i){
        event = i;
    }
    public  void SetTitle(String i){
         title = i;
    }
    public void SetBegin(String i){
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = null;
        try {
            d = dt1.parse(i);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        begin = d;
    }
    public void SetEnd(String i){
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d = null;
        try {
            d = dt1.parse(i);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        end = d;
    }


}
