package com.example.silencer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

public class CalendarEvents extends AppCompatActivity {
    // a static variable to get a reference of our application context
    public static Context contextOfApplication;

    public static Context getContextOfApplication() {
        return contextOfApplication;
    }

    public static boolean collapse = true;
    public static long year = 31536000000L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_events);
        contextOfApplication = getApplicationContext();
        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(Calendar.EXTRA_MESSAGE);
        collapse = true;
        getDataFromCalendarTable(message);
    }

    public void back(View view) {
        Intent intt = new Intent(this, Calendar.class);
        startActivity(intt);
    }

    public void collapse(View view) {
        collapse = true;
        Button ll = findViewById(R.id.collapse);
        ll.setText(R.string.showallevents);
        ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showAll(view);
            }
        });
        Intent intent = getIntent();
        String message = intent.getStringExtra(Calendar.EXTRA_MESSAGE);
        getDataFromCalendarTable(message);
    }

    public void showAll(View view) {
        collapse = false;
        Button ll = findViewById(R.id.collapse);
        ll.setText(R.string.collapseevents);
        ll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                collapse(view);
            }
        });
        Intent intent = getIntent();
        String message = intent.getStringExtra(Calendar.EXTRA_MESSAGE);
        getDataFromCalendarTable(message);
    }


    public void getDataFromCalendarTable(String id) {

        String[] EVENT_PROJECTION = new String[]{
                CalendarContract.Calendars._ID,
                CalendarContract.Calendars.ACCOUNT_NAME,
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
        };

        Cursor cur = null;
        ContentResolver cr = getContentResolver();
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        // Submit the query and get a Cursor object back.
        cur = cr.query(uri, EVENT_PROJECTION, "(" + CalendarContract.Calendars._ID + " = " + id + ")", null, null);

        ArrayList<CalendarEventInstance> data = new ArrayList<CalendarEventInstance>();

        // Use the cursor to step through the returned records
        while (cur.moveToNext()) {
            String NAME = null;
            // Get the field values

            int calID = cur.getInt(0);
            String ACCOUNT_NAME = cur.getString(1);
            NAME = cur.getString(2);

            if (NAME == "" || NAME == null)
                NAME = ACCOUNT_NAME;

            // set name of calendar
            TextView textView = findViewById(R.id.name);
            textView.setText(NAME);

            if (collapse) {
                data = GetCollapsedEvents(cr, id);
                FragmentManager fm = getSupportFragmentManager();
                ListView myList = (ListView) findViewById(R.id.listView);
                CalendarEventsCollapsedListAdapter adapter = new CalendarEventsCollapsedListAdapter(this, R.layout.calendar_event_view, data, fm);
                myList.setAdapter(adapter);
            } else {
                data = GetAllEvents(cr, id);
                FragmentManager fm = getSupportFragmentManager();
                ListView myList = (ListView) findViewById(R.id.listView);
                CalendarEventListAdapter adapter = new CalendarEventListAdapter(this, R.layout.remainder_list_view, data, fm);
                myList.setAdapter(adapter);
            }
        }
    }

    private ArrayList<CalendarEventInstance> GetAllEvents(ContentResolver cr, String id) {

            // fill events
            Date now = new Date();
            Uri.Builder builder = Uri.parse("content://com.android.calendar/instances/when").buildUpon();
            ContentUris.appendId(builder, now.getTime());
            ContentUris.appendId(builder, now.getTime() +  ((year)));

        ArrayList<CalendarEventInstance> data = new ArrayList<CalendarEventInstance>();

            Cursor eventCursor = cr.query(builder.build(),
                    new String[]{CalendarContract.Instances._ID, "title", "begin", "end", "startDay",
                            "endDay", "startMinute", "endMinute"}, "( " + CalendarContract.Instances.CALENDAR_ID + " = " + id + ")",
                    null, "startDay ASC, startMinute ASC");

            while (eventCursor.moveToNext()) {
                final int EVENT_ID = eventCursor.getInt(0);
                final String TITLE = eventCursor.getString(1);
                final Date begin = new Date(eventCursor.getLong(2));
                final Date end = new Date(eventCursor.getLong(3));

                data.add(new CalendarEventInstance(EVENT_ID, TITLE, begin, end));

            }

            return  data;
    }


    private   ArrayList<CalendarEventInstance>  GetCollapsedEvents(ContentResolver cr, String id) {
        // fill events
        Uri.Builder builder = CalendarContract.Events.CONTENT_URI.buildUpon();
        Date now = new Date();
        //duration if the event is recurring
        //rrule or rdate if the event is recurring
        Cursor eventCursor = cr.query(builder.build(),
                new String[]{CalendarContract.Events._ID, "title", CalendarContract.Events.DTEND,
                        CalendarContract.Events.DTSTART, "duration", "rrule"
                        }, "( " + CalendarContract.Events.CALENDAR_ID + " = " + id + " )",
                null, "dtstart ASC");
        ArrayList<CalendarEventInstance> data = new ArrayList<CalendarEventInstance>();

        while (eventCursor.moveToNext()) {
            final int EVENT_ID = eventCursor.getInt(0);
            String TITLE = eventCursor.getString(1);
            final long DTEND = eventCursor.getLong(2);
            final long DTSTART = eventCursor.getLong(3);
            final String duration = eventCursor.getString(4);
            final String rrule = eventCursor.getString(5);

            if(DTEND != 0 && DTEND < now.getTime())
                continue;
            if(DTSTART > now.getTime() + year )
                continue;
            if(rrule != null) {
                String[] rr = rrule.split(";");
                for (int i = 0; i < rr.length; i++) {
                    String[] r = rr[i].split("=");
                    if (r.length > 0) {
                        if (r[0].equals("FREQ")) {
                            TITLE += " " + r[1];
                        }
                    }
                }
            }

            data.add(new CalendarEventInstance(EVENT_ID, TITLE));

        }
        return  data;

    }


}
