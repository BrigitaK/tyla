package com.example.silencer;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class AssignDialog extends DialogFragment {
    public static long year = 31536000000L;

    DatabaseHandler databaseHandler;

    public static AssignDialog newInstance(String title, String id, String type) {

        AssignDialog frag = new AssignDialog();
        Bundle args = new Bundle();
        args.putString("Title", title);
        args.putString("ID", id);
        args.putString("Type", type);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        databaseHandler = new DatabaseHandler(CalendarEvents.getContextOfApplication());
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.dialogStyle);
        final String title = getArguments().getString("Title");
        final String  ID = getArguments().getString("ID");
        final String  TYPE = getArguments().getString("Type");

        builder.setTitle(getResources().getString(R.string.assingseventtitle));
        builder.setMessage( title )
                .setPositiveButton( getResources().getString(R.string.assigneventyes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        switch (TYPE) {
                            case "0": AssignEvent(ID);
                                break;
                            case "1": AssignInstance(ID);
                                break;
                            default:
                                break;
                        }
                    }
                })
                .setNegativeButton(getResources().getString(R.string.assigneventno), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.dismiss();
                    }
                });


        // Create the AlertDialog object and return it
        AlertDialog alert = builder.create();
        alert.show();
        alert.getWindow().getAttributes();

        TextView textView = alert.findViewById(android.R.id.message);
        textView.setTextSize(20);
        return  alert;
    }

    private void AssignEvent(String ID){

        Context applicationContext = CalendarEvents.getContextOfApplication();
        Date now = new Date();
        Uri.Builder builder = Uri.parse("content://com.android.calendar/instances/when").buildUpon();
        ContentUris.appendId(builder, now.getTime() );
        ContentUris.appendId(builder, now.getTime() + year  );
        ContentResolver cr =  applicationContext.getContentResolver();
        Cursor eventCursor = cr.query(builder.build(),
                new String[]  { CalendarContract.Instances._ID, "title", "begin", "end", CalendarContract.Instances.EVENT_ID },
                "( EVENT_ID = " + ID + ")",
                null, null);

        while(eventCursor.moveToNext())
        {
            final int EVENT_ID =  eventCursor.getInt(0);
            final String TITLE =  eventCursor.getString(1);
            final Date begin = new Date(eventCursor.getLong(2));
            final Date end = new Date(eventCursor.getLong(3));

            Remainder entry = new Remainder(EVENT_ID, TITLE, begin , end);
            databaseHandler.Add(entry);

        }
    }

    private void AssignInstance(String ID){
        Context applicationContext = CalendarEvents.getContextOfApplication();
        Date now = new Date();
        Uri.Builder builder = Uri.parse("content://com.android.calendar/instances/when").buildUpon();
        ContentUris.appendId(builder, now.getTime() );
        ContentUris.appendId(builder, now.getTime() + year  );
        ContentResolver cr =  applicationContext.getContentResolver();
        Cursor eventCursor = cr.query(builder.build(),
                new String[]  { CalendarContract.Instances._ID, "title", "begin", "end", CalendarContract.Instances.EVENT_ID },
                "( Instances._id = " + ID + ")",
                null, null);

        while(eventCursor.moveToNext())
        {
            final int EVENT_ID =  eventCursor.getInt(0);
            final String TITLE =  eventCursor.getString(1);
            final Date begin = new Date(eventCursor.getLong(2));
            final Date end = new Date(eventCursor.getLong(3));
            Remainder entry = new Remainder(EVENT_ID, TITLE, begin , end);
            databaseHandler.Add(entry);

        }
    }


}