package com.example.silencer;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import java.nio.channels.Channel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import static androidx.core.app.NotificationCompat.DEFAULT_SOUND;

public class SilenceService extends Service {
    public Runnable mRunnable = null;
    public boolean silenced = false;
    public Remainder next = new Remainder();
    public Remainder current;
    private static final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    public SilenceService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final Handler mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                DatabaseHandler myDBHelper = new DatabaseHandler(getApplicationContext());
                Remainder remainder = myDBHelper.getCurrentSilenceTime();
                myDBHelper.deleteOld();
                if(!silenced && remainder.Event() != 0){
                    SilencePhone(remainder);
                } else if( next.Event() == 0 && silenced && remainder.Event() != current.Event()){
                   next = remainder;
                }
                mHandler.postDelayed(mRunnable, 20 * 1000);
            }
        };
        mHandler.postDelayed(mRunnable, 5 * 1000);

       // return super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    public void SilencePhone(Remainder remainder) {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        PowerManager powerManager = (PowerManager) getApplicationContext().getSystemService(POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !notificationManager.isNotificationPolicyAccessGranted()) {
            Intent intent = new Intent(
                    android.provider.Settings
                            .ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
            startActivity(intent);
        } else {
            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
            silenced = true;
            current = remainder;
            // Notification stuff
            SimpleDateFormat dt1 = new SimpleDateFormat("HH:mm");
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
            builder.setSmallIcon(R.drawable.icon);
            builder.setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE);
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.icon));
            builder.setContentTitle(getResources().getString(R.string.silencedPhone));
            builder.setContentText(getResources().getString(R.string.silencedPhoneUntil) + " " + dt1.format(remainder.End()));
            builder.setLights(0, 1000, 1000);
            builder.setDefaults(DEFAULT_SOUND);
            TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(getApplicationContext());
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            taskStackBuilder.addParentStack(MainActivity.class);
            taskStackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(100, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            final long[] DEFAULT_VIBRATE_PATTERN = {0, 250, 250, 250};
            builder.setVibrate(DEFAULT_VIBRATE_PATTERN);
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            builder.setLights(Color.WHITE, 2000, 3000);
            // This is the answer to OP's question, set the visibility of notification to public.
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            notificationManager.notify(10, builder.build());

            long time = remainder.End().getTime() - remainder.Begin().getTime();

            soundOnDelay(time);
        }
    }

    public void SoundOn() {
        AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        silenced = false;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        builder.setSmallIcon(R.drawable.icon);
        builder.setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.icon));
        builder.setContentTitle(getResources().getString(R.string.unsilencedPhone));
        builder.setLights(0, 1000, 1000);
        builder.setDefaults(DEFAULT_SOUND);
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(getApplicationContext());
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        taskStackBuilder.addParentStack(MainActivity.class);
        taskStackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(110, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        final long[] DEFAULT_VIBRATE_PATTERN = {0, 250, 250, 250};
        builder.setVibrate(DEFAULT_VIBRATE_PATTERN);
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        builder.setLights(Color.WHITE, 2000, 3000);
        // This is the answer to OP's question, set the visibility of notification to public.
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(10, builder.build());

        current = next;
        next = new Remainder();
        if(current.Event() != 0){
            SilencePhone(current);
        }
    }

    public void soundOnDelay(long time) {
        Runnable delayedTask = new Runnable() {
            @Override
            public void run() {
                SoundOn();
            }
        };
        mainThreadHandler.postDelayed(delayedTask, time);
    }


}