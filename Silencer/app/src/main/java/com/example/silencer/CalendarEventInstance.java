package com.example.silencer;

import java.util.Date;

public class CalendarEventInstance {
    public int Id ;
    public String Title ;
    public Date Begin;
    public Date End ;

    public CalendarEventInstance( int _Id, String _Title,  Date _Begin,  Date _End){
        Id = _Id;
        Title = _Title;
        End =  _End;
        Begin =  _Begin;
    }
    public CalendarEventInstance( int _Id, String _Title){
        Id = _Id;
        Title = _Title;
    }
}
