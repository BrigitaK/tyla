package com.example.silencer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class Calendar extends AppCompatActivity {
    ArrayList<String> listItems= new ArrayList<String>();
    ArrayAdapter<String> adapter;

    public static final String EXTRA_MESSAGE = "com.example.silencer.MESSAGE";

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                getDataFromCalendarTable();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALENDAR)
                == PackageManager.PERMISSION_DENIED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{ Manifest.permission.READ_CALENDAR },
                    2);
        } else {
            getDataFromCalendarTable();
        }

    }

    public void home(View view){
        Intent intt = new Intent(this, MainActivity.class);
        startActivity(intt);
    }

    public void getDataFromCalendarTable() {

        String[] EVENT_PROJECTION = new String[] {
                CalendarContract.Calendars._ID,
                CalendarContract.Calendars.ACCOUNT_NAME,
                CalendarContract.Calendars.NAME,
                CalendarContract.Calendars.CALENDAR_COLOR,
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME
        };

        // Run query
        Cursor cur = null;
        ContentResolver cr = getContentResolver();
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        // Submit the query and get a Cursor object back.
        cur = cr.query(uri, EVENT_PROJECTION, null, null, null);

        ArrayList<CalendarInstance> data = new ArrayList<CalendarInstance>();

        // Use the cursor to step through the returned records
        while (cur.moveToNext()) {
            int calID = 0;
            String ACCOUNT_NAME = null;
            String NAME = null;
            String CALENDAR_DISPLAY_NAME = null;
            int CALENDAR_COLOR = 0;

            // Get the field values
            calID = cur.getInt(0);
            ACCOUNT_NAME = cur.getString(1);
            NAME = cur.getString(2);
            CALENDAR_COLOR = cur.getInt(3);
            CALENDAR_DISPLAY_NAME = cur.getString(4);


            Date now = new Date();
            Uri.Builder builder = Uri.parse("content://com.android.calendar/instances/when").buildUpon();
            ContentUris.appendId(builder, now.getTime() );
            ContentUris.appendId(builder, now.getTime() + ( 365 * 24 * 60 * 60 * 1000));

            Cursor eventCursor = cr.query(builder.build(),
                    new String[]  { CalendarContract.Instances.EVENT_ID, "title", "begin", "end",  "startDay",
                            "endDay",  "startMinute" , "endMinute" }, "( " + CalendarContract.Instances.CALENDAR_ID +" = " + calID + ")",
                    null, "startDay ASC, startMinute ASC");
            if(eventCursor.getCount() <= 0)
                continue;

            if(CALENDAR_DISPLAY_NAME == "" || CALENDAR_DISPLAY_NAME == null)
                CALENDAR_DISPLAY_NAME = " ";

             if(CALENDAR_DISPLAY_NAME.equals(ACCOUNT_NAME))
                ACCOUNT_NAME = "";


            data.add(new CalendarInstance(calID, CALENDAR_DISPLAY_NAME, ACCOUNT_NAME));
             /*
            Button btn = new Button(this);
            btn.setId(calID);
            btn.setText(CALENDAR_DISPLAY_NAME + " " + ACCOUNT_NAME + " " ) ;
            btn.setTag(calID);
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent intt = new Intent(getApplicationContext(), CalendarEvents.class);
                    intt.putExtra(EXTRA_MESSAGE, view.getTag().toString() );
                    startActivity(intt);
                }
            });

            btn.setTextColor(Color. parseColor("#B3C5CE"));

                btn.setBackgroundColor(Color. parseColor("#2AE3EBE9"));

            btn.setTextSize(12);
            btn.setGravity(Gravity.CENTER_VERTICAL);
            btn.setPadding( 15, 1 , 15 ,0);
            LinearLayoutCompat.LayoutParams lp = new LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
            LinearLayout ll = findViewById(R.id.llayout);
            ll.addView(btn, lp);

            */

        }


        FragmentManager fm = getSupportFragmentManager();
        ListView myList = (ListView) findViewById(R.id.listView);
        CalendarListAdapter adapter = new CalendarListAdapter(this, R.layout.calendar_list_view, data, fm);
        myList.setAdapter(adapter);


    }

}