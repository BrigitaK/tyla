package com.example.silencer;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

public class CalendarEventsCollapsedListAdapter extends ArrayAdapter<CalendarEventInstance> {

    private Context _context;
    int _resource;
    FragmentManager _fragmentManager;
    public static final String EXTRA_MESSAGE = "com.example.silencer.MESSAGE";

    public CalendarEventsCollapsedListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CalendarEventInstance> objects, FragmentManager fragmentManager) {
        super(context, resource, objects);
        _context = context;
        _resource = resource;
        _fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String title = getItem(position).Title;
        int id  = getItem(position).Id;

        LayoutInflater inflater = LayoutInflater.from(_context);
        convertView = inflater.inflate(_resource, parent, false);

        TextView Title = (TextView) convertView.findViewById(R.id.calendarEvent);
        Title.setText(title);
        Title.setTag(id);
        Title.setId(id);
        Title.setTag(R.id.event_Id, id);
        Title.setTag(R.id.event_Name, title);
        Title.setTag(R.id.event_Type, 0);
        Title.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AssignDialog(view);
            }
        });

        return convertView;
    }

    public void AssignDialog(View view){
        String id = view.getTag(R.id.event_Id).toString();
        String name = view.getTag(R.id.event_Name).toString();
        String type = view.getTag(R.id.event_Type).toString();

        AssignDialog assignDialog = AssignDialog.newInstance(name, id, type);
        assignDialog.show(_fragmentManager, "fragment_assign_event");
    }

}
