package com.example.silencer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.Nullable;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "remainders.db";

    private static String TABLE_NAME = "remainder";

    private final static String KEY_ID = "id";
    private final static String KEY_EVENT_ID = "event";
    private final static String KEY_TITLE = "title";
    private final static String KEY_BEGIN = "begining";
    private final static String KEY_END = "ending";


    public DatabaseHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " ( " +
                 KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                 KEY_EVENT_ID + " INTEGER, " +
                 KEY_TITLE + " TEXT, " +
                 KEY_BEGIN + " datetime , " +
                 KEY_END + " datetime )";

        db.execSQL(createTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP  TABLE IF  EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean Add(Remainder remainder) {
        if (getEntryByEventId(remainder.Event()).Event() != 0) {
            return false;
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        contentValues.put(KEY_EVENT_ID, remainder.Event());
        contentValues.put(KEY_TITLE, remainder.Title());
        contentValues.put(KEY_BEGIN, dt1.format(remainder.Begin()));
        contentValues.put(KEY_END, dt1.format(remainder.End()));


        long id = db.insert(TABLE_NAME, null, contentValues);
        db.close();
        return id > 0;
    }


    public Remainder getEntryByEventId(int id)
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME
                + " WHERE  " + KEY_EVENT_ID + " =  "+ id +" ORDER BY  datetime(begining) ASC";
        Cursor cursor = db.rawQuery(query, null);

        Remainder entry = new Remainder();
        if(cursor != null)
        {

            if(cursor.moveToFirst())
            {
                do {
                    entry.SetId(cursor.getInt(0));
                    entry.SetEvent(cursor.getInt(1));
                    entry.SetTitle(cursor.getString(2));
                    entry.SetBegin(cursor.getString(3));
                    entry.SetEnd(cursor.getString(4));
                } while(cursor.moveToNext());
            }
        }
        cursor.close();
        db.close();
        return entry;
    }
    public  void deleteOld(){

        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = dt1.format( new Date(System.currentTimeMillis() - 5 * 60 * 1000));

        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, KEY_BEGIN + "<= Datetime('"+ now +"')" , null);
        db.close();
    }

    public Remainder getEntry(int id)
    {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = null;

        cursor = db.query(TABLE_NAME,
                new String[] { KEY_ID,  KEY_EVENT_ID, KEY_TITLE, KEY_BEGIN, KEY_END},
                KEY_ID + "=?", new String[] { Integer.toString(id) }, null, null, null);

        Remainder entry = new Remainder();
        if(cursor != null)
        {
            if(cursor.moveToFirst())
            {
                do {
                    entry.SetId(cursor.getInt(0));
                    entry.SetEvent(cursor.getInt(1));
                    entry.SetTitle(cursor.getString(2));
                    entry.SetBegin(cursor.getString(3));
                    entry.SetEnd(cursor.getString(4));
                } while(cursor.moveToNext());
            }
        }
        cursor.close();
        db.close();
        return entry;
    }

    public ArrayList<Remainder> getAllEntries()
    {
        ArrayList<Remainder> entries = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = dt1.format( new Date(System.currentTimeMillis() - 2 * 60 * 1000));
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE  " + KEY_BEGIN +
                " >=  Datetime('"+ now +"') ORDER BY  datetime(begining) ASC";
       // String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY  datetime(begining) DESC";
        Cursor cursor = db.rawQuery(query, null);

        if(cursor != null)
        {
            if(cursor.moveToFirst())
            {
                do {
                    Remainder entry = new Remainder();
                    entry.SetId(cursor.getInt(0));
                    entry.SetEvent(cursor.getInt(1));
                    entry.SetTitle(cursor.getString(2));
                    entry.SetBegin(cursor.getString(3));
                    entry.SetEnd(cursor.getString(4));
                    entries.add(entry);
                } while(cursor.moveToNext());
            }
        }
        cursor.close();
        db.close();
        return entries;
    }

    public void deleteEntry(String id)
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ID + "=?", new String[]{id});
        db.close();
    }

    public void updateEntry(Remainder entry)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_EVENT_ID, entry.Event());
        cv.put(KEY_TITLE, entry.Title());
        cv.put(KEY_BEGIN, entry.Begin().toString());
        cv.put(KEY_END, entry.End().toString());
        db.update(TABLE_NAME, cv, KEY_ID + "=?", new String[] { Integer.toString(entry.Id()) });
        db.close();
    }

    public  Remainder getCurrentSilenceTime()
    {
        Remainder entries = new Remainder();
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = dt1.format( new Date(System.currentTimeMillis() -  60 * 1000));
        String now2 = dt1.format( new Date(System.currentTimeMillis() +  30 * 1000));
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE  " + KEY_BEGIN + " >=  Datetime('"+ now +"')  AND    " + KEY_BEGIN + "  <=  Datetime('"+ now2 +"') ORDER BY  datetime(begining) ASC";

        Cursor cursor = db.rawQuery(query, null);
        if(cursor != null)
        {
            if(cursor.moveToFirst())
            {
                do {
                    Remainder entry = new Remainder();
                    entry.SetId(cursor.getInt(0));
                    entry.SetEvent(cursor.getInt(1));
                    entry.SetTitle(cursor.getString(2));
                    entry.SetBegin(cursor.getString(3));
                    entry.SetEnd(cursor.getString(4));

                    cursor.close();
                    db.close();
                    return entry;
                } while(cursor.moveToNext());
            }
        }
        cursor.close();
        db.close();
        return entries;
    }

}
