package com.example.silencer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import java.util.Locale;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Settings extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "LOCALE";

    public static final String LT = " lt_";
    public static final String EN = "en_";

    public static Context contextOfApplication;
    public static final String MY_PREFERENCES = "GamePreferences";

    public static Context getContextOfApplication() {
        return contextOfApplication;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        contextOfApplication = getApplicationContext();
        CheckLanguage();
    }

    public void CheckLanguage(){
        SharedPreferences sharedPrefs = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        String locale = sharedPrefs.getString(EXTRA_MESSAGE, null);
        Button en = findViewById(R.id.enbtn);
        Button lt = findViewById(R.id.ltbtn);
        Locale current = getResources().getConfiguration().locale;
        if (locale != null) {
            if (locale.equals(LT) ) {
                en.setBackgroundColor(getResources().getColor(R.color.bcklistcolor));
                lt.setBackgroundColor(getResources().getColor(R.color.bckTextinv));
                if(!current.getDisplayLanguage().equals(LT))
                    setLocaleLt();
            } else if (locale.equals(EN)){
                en.setBackgroundColor(getResources().getColor(R.color.bckTextinv));
                lt.setBackgroundColor(getResources().getColor(R.color.bcklistcolor));
                if(!current.getDisplayLanguage().equals("English"))
                    setLocaleEn();
            }
        }
        else {
            if(current.getDisplayLanguage().equals("English")){
                en.setBackgroundColor(getResources().getColor(R.color.bckTextinv));
                lt.setBackgroundColor(getResources().getColor(R.color.bcklistcolor));
            } else {
                en.setBackgroundColor(getResources().getColor(R.color.bcklistcolor));
                lt.setBackgroundColor(getResources().getColor(R.color.bckTextinv));
            }
        }
    }

    public void home(View view){
        Intent intt = new Intent(this, MainActivity.class);
        startActivity(intt);
    }

    public void setLocaleEn(View view) {
        SharedPreferences.Editor sharedPrefEditor = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE).edit();
        sharedPrefEditor.putString(EXTRA_MESSAGE, EN);
        sharedPrefEditor.commit();
        Locale myLocale = Locale.ENGLISH;
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, Settings.class);
        startActivity(refresh);
        finish();
    }
    public void setLocaleEn() {
        Locale myLocale = Locale.ENGLISH;
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, Settings.class);
        startActivity(refresh);
        finish();
    }

    public void setLocaleLt(View view) {
        SharedPreferences.Editor sharedPrefEditor = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE).edit();
        sharedPrefEditor.putString(EXTRA_MESSAGE, LT);
        sharedPrefEditor.commit();
        Locale myLocale = new Locale(LT);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, Settings.class);
        startActivity(refresh);
        finish();
    }

    public void setLocaleLt() {
        Locale myLocale = new Locale(LT);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, Settings.class);
        startActivity(refresh);
        finish();
    }





}
