package com.example.silencer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CheckLanguage();

        startService(new Intent(this, SilenceService.class));
    }

    public void aboutPage(View view){
        Intent intt = new Intent(this, About.class);
       startActivity(intt);
    }
    public void calendarPage(View view){
        Intent intt = new Intent(this, Calendar.class);
        startActivity(intt);
    }
    public void settingsPage(View view){
        Intent intt = new Intent(this, Settings.class);
        startActivity(intt);
    }
    public void remaindersPage(View view){
        Intent intt = new Intent(this, Remainders.class);
        startActivity(intt);
    }

    public  void CheckLanguage(){
        SharedPreferences sharedPrefs = getSharedPreferences(Settings.MY_PREFERENCES, Context.MODE_PRIVATE);
        String locale = sharedPrefs.getString(Settings.EXTRA_MESSAGE, null);
        Locale current = getResources().getConfiguration().locale;
        if (locale != null) {
            if (locale.equals(Settings.LT) ) {
                if(!current.getDisplayLanguage().equals(Settings.LT))
                    setLocaleLt();
            } else if (locale.equals(Settings.EN)){
                if(!current.getDisplayLanguage().equals("English"))
                    setLocaleEn();
            }
        }
    }
    public void setLocaleLt() {
        Locale myLocale = new Locale(Settings.LT);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        startActivity(refresh);
        finish();
    }
    public void setLocaleEn() {
        Locale myLocale = Locale.ENGLISH;
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        startActivity(refresh);
        finish();
    }



}
